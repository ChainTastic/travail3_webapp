from bd import obtenir_connexion
from blueprints.comptes.models.utilisateur_model import UtilisateurModel
from flask import (
    Blueprint,
    render_template,
    url_for,
    request,
    redirect,
    session,
    flash
)

comptes = Blueprint('comptes', __name__, template_folder='templates')


@comptes.route('/utilisateurs/creer-compte')
def creation_compte():
    est_admin = False
    if "utilisateur" in session:
        user = session['utilisateur']
        connexion = obtenir_connexion()
        curseur = connexion.cursor()
        curseur.execute("SELECT est_admin FROM utilisateurs WHERE identifiant = %s", (user,))
        est_admin, = curseur.fetchone()
    return render_template('creer.html', estAdmin=est_admin)


@comptes.route('/utilisateurs/creer-compte', methods=['POST'])
def creation_compte_post():
    identifiant = request.form.get('id')
    mot_de_passe = request.form.get('mot-passe')
    est_admin = 'admin' in request.form or False
    model = UtilisateurModel(identifiant, mot_de_passe, est_admin)
    messages = model.valider_creation()
    if len(messages) > 0:
        return render_template('creer.html', messages=messages)
    model.enregistrer()
    if "utilisateur" in session:
        return redirect(url_for('accueil', id=session["utilisateur"]))
    return render_template('accueil.html')


@comptes.route("/authentification/connexion")
def authentification():
    return render_template("authentification.html")


@comptes.route("/authentification/connexion", methods=["POST"])
def authentification_post():
    args = request.args
    identifiant = request.form.get('id')
    mot_de_passe = request.form.get('mot-passe')
    model = UtilisateurModel(identifiant, mot_de_passe)
    messages = model.valider_authentification()
    if messages:
        for message in messages:
            flash(message)
        return render_template("authentification.html")
    session["utilisateur"] = model.identifiant
    if "page_voulue" in args:
        return redirect(request.args.get('page_voulue'))
    return redirect(url_for('accueil', id=identifiant))


@comptes.route("/authentification/deconnexion")
def deconnexion():
    flash('Vous vous êtes déconecté')
    session.pop('utilisateur', None)
    return redirect(url_for("comptes.authentification"))
