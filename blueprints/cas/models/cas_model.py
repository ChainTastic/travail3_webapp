from flask_sqlalchemy import SQLAlchemy

from bd import obtenir_connexion

db = SQLAlchemy()


class Region(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(80), unique=True, nullable=False)
    cas = db.relationship('Cas', backref='region')

    def __repr__(self):
        return '<Nom %r>' % self.nom


class Cas(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, nullable=False)
    region_id = db.Column(db.Integer, db.ForeignKey('region.id'))
    nom = db.Column(db.String(80), nullable=False)
    prenom = db.Column(db.String(80), nullable=False)

    def __repr__(self):
        return {'id': self.id, 'nom': self.nom, 'prenom': self.prenom}

    def valider_creation(self):
        try:
            messages = []
            if self.champs_sont_vides():
                messages.append("L'identifiant et le mot de passe ne doivent pas être vides")
            return messages
        except Exception as e:
            pass

    def champs_sont_vides(self):
        if not self.nom or not self.prenom:
            return "Le nom et le prénom ne doivent pas être vides"

    def enregistrer(self):
        connexion = obtenir_connexion()
        try:
            curseur = connexion.cursor()
            curseur.execute(
                'INSERT INTO cas (date, region_id, nom, prenom) VALUES (%s, %s, %s, %s)',
                (self.date, self.region_id, self.nom, self.prenom))
            connexion.commit()
        except Exception as e:
            pass
        finally:
            connexion.close()



