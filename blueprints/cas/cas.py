from datetime import date

from sqlalchemy import func

from bd import obtenir_connexion
from blueprints.cas.models.cas_model import Cas, Region, db
from flask import (
    Blueprint,
    render_template,
    url_for,
    request,
    redirect,
    session,
    abort,
    flash
)

cas = Blueprint('cas', __name__,
                template_folder='templates')


@cas.route('/cas/creer-cas')
def creation_cas():
    if 'utilisateur' not in session:
        abort(401)
    return render_template('enregistrement.html', date=date.today(), regions=Region.query.all())


@cas.route('/cas/creer-cas', methods=['POST'])
def creation_cas_post():
    nom = request.form.get('nom')
    prenom = request.form.get('prenom')
    date_creation = request.form.get('date')
    region = request.form['region']
    model = Cas(date=date_creation, region_id=region, nom=nom, prenom=prenom)
    messages = model.valider_creation()
    if len(messages) > 0:
        return render_template('enregistrement.html', messages=messages, date=date.today(), regions=Region.query.all())
    model.enregistrer()
    return redirect(url_for('accueil', id=session["utilisateur"]))


@cas.route("/cas/liste")
def lister():
    if "utilisateur" in session:
        utilisateur = session['utilisateur']
        connexion = obtenir_connexion()
        curseur = connexion.cursor()
        curseur.execute("SELECT est_admin FROM utilisateurs WHERE identifiant = %s", (utilisateur,))
        est_admin, = curseur.fetchone()
        connexion.close()
        return render_template("liste.html", liste_cas=Cas.query.all(), estAdmin=est_admin)
    return render_template("liste.html", liste_cas=Cas.query.all())


@cas.route("/cas/liste_par_region")
def lister_par_region():
    donnees = Region.query.with_entities(Region.nom, func.count(Cas.region_id)).join(Cas).group_by(Region.id).all()
    return render_template("liste_cas_region.html", donnees_regions=donnees)


@cas.route("/cas/<int:cas_id>/supprimer")
def supprimer_cas(cas_id):
    connexion = obtenir_connexion()
    curseur = connexion.cursor()
    try:
        if 'utilisateur' not in session:
            abort(401)
        uid = session['utilisateur']
        curseur.execute("SELECT est_admin FROM utilisateurs WHERE identifiant = %s", (uid,))
        est_admin, = curseur.fetchone()
        if est_admin is None or est_admin == 0:
            abort(403)
        elem = Cas.query.filter_by(id=cas_id).first()
        db.session.delete(elem)
        db.session.commit()
        return redirect(url_for('cas.lister'))
    except Exception as e:
        #logger.error(str(e))
        abort(500)


@cas.errorhandler(401)
def erreur404(error):
    return render_template("erreurs/page401.html", page=request.url), 401
