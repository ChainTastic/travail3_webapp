from flask import (
    Flask,
    render_template,
    url_for,
    request,
    redirect,
    session,
    make_response,
    flash,
    abort
)

from bd import obtenir_connexion
from blueprints.comptes.comptes import comptes
from blueprints.cas.cas import cas
from blueprints.cas.models.cas_model import Cas, Region, db


def creer_application():
    app_flask = Flask(__name__)
    app_flask.secret_key = "gb[>WZy.pR9*I*F"
    app_flask.register_blueprint(comptes)
    app_flask.register_blueprint(cas)
    app_flask.config['BABEL_DEFAULT_LOCALE'] = ['fr']
    app_flask.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:@localhost:3306/420-05c_tp3'
    app_flask.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app_flask)
    with app_flask.app_context():
        db.create_all()
    return app_flask


app = creer_application()


@app.route('/')
def index():
    try:
        if "utilisateur" in session:
            identifiant = session['utilisateur']
            connexion = obtenir_connexion()
            curseur = connexion.cursor()
            curseur.execute("SELECT est_admin FROM utilisateurs WHERE id = %s", (identifiant,))
            est_admin, = curseur.fetchone()
            connexion.close()
            resp = make_response(redirect(url_for('accueil', id=identifiant, estAdmin=est_admin)))
            return resp
        resp = make_response(redirect(url_for('accueil')))
        return resp
    except Exception as e:
        pass


@app.route('/accueil')
def accueil():
    identifiant = request.args.get('id')
    est_admin = request.args.get('estAdmin')
    return render_template('accueil.html', estAdmin=est_admin, identifiant=identifiant)


if __name__ == '__main__':
    app.run()
