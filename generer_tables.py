from datetime import date

from app import app
from flask_sqlalchemy import SQLAlchemy
from blueprints.cas.models.cas_model import Region, Cas

db = SQLAlchemy(app)


regions = {
    "Bas-Saint-Laurent",
    "Saguenay-Lac-Saint-Jean",
    "Capitale-Nationale",
    "Mauricie",
    "Estrie",
    "Montréal",
    "Outaouais",
    "Abitibi-Témiscamingue",
    "Côte-Nord",
    "Nord-du-Québec",
    "Gaspésie–Îles-de-la-Madeleine-",
    "Chaudière-Appalaches",
    "Laval",
    "Lanaudière",
    "Montérégie",
    "Centre-du-Québec"
}

for i in range(len(regions)):
    cas = Cas(date=date.today(), region_id=i+1, nom=str(i), prenom='Cas_test')
    db.session.add(cas)

# for region in regions:
#     reg = Region(nom=region)
#     db.session.add(reg)

db.session.commit()
